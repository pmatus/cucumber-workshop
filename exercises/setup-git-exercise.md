# Setup GIT repository

In this exercise a new git repository is setup and this project forked into the newly created repository. This will create the capability to create a continuous delivery pipeline tied to the newly created account using the Gitlab CI/CD capabilities.

## Create an account

This step can be skipped if an GitLab account already exists. Take these steps forcreating a GitLab account:

1. Go to the [GitLab sign-up page](https://gitlab.com/users/sign_in) and select register. ![Signup](images/signup.png)
2. Fill in the form with correct credentials and a confirmation e-mail will be sent.
3. Confirm the account by clicking the confirm link in the e-mail.
4. Now sign in with the newly create account on the [GitLab sign-up page](https://gitlab.com/users/sign_in) and select sign-in.

## Fork the project

Forking is a means of creating a copy of a project where a project can be extended and reshaped without touching the original project. Take these steps to fork the project:

1. Go to [Cucumber workshop project](https://gitlab.com/hylke1982/cucumber-workshop)
2. Select fork button and select the newly created account. ![Fork](images/fork.png)
3. The project will be forked and will be available within one minute

Now it is time to create a local clone of the project.

## Clone the project

Cloning the project means creating a local copy of the repository. For cloning a project take these steps:

1. First go to the forked version of the project on GitLab. Link will be https://gitlab.com/**gitlab-account**/cucumber-workshop.
2. Open a terminal window for instance with git-bash or powershell and go to a directory where the project can be stored.
3. Clone the project with following command. `git clone https://gitlab.com/**gitlab-account**/cucumber-workshop`
4. Go into the project with `cd cucumber-workshop`

## Crash course GIT

GIT is a distributed source control system, meaning that source code can be pushed and pulled from and to different sources. These basic commands allow for working with GIT:

* First clone of a project. `git clone url-of-the-repo`
* After changing, adding and deleting files execute `git add .` this will add the changes to the local changes.
* After adding changes the command `git commit -am "commit message"` will commit the changes to the local copy.
* These commit can be pushed then from the local copy of the repository to the remote repository by using the command `git push`.
* If working together on a project, changes from other users on the remote repositry can be received with `git pull` 

The is very simplified summary of GIT commands, the possibilites with GIT are more extensive like branch, stashing and many more useful features. 
