# Scenario outline

A scenario outline is great way to do specification by example. This means by providing examples the functionality is 
validated and clarified.

## Setting up a scenario outline

In the previous exercises show the there is already a complete library of glue code that can be used to validate how to 
application works. These steps can be reused in a scenario out, by following these steps:

- Open the **src/test/resources/features/triangle-calculation.feature** file with the editor of choice.
- Add the following block of code to the feature and save the file
```
  Scenario Outline: Triangles
    Given Input calculator is a available
    When I enter for side 1 value <side value 1>
    And I enter for side 2 value <side value 2>
    And I enter for side 3 value <side value 3>
    Then the triangle is qualified as '<triangle type>'
    Examples:
      | side value 1 | side value 2 | side value 3 | triangle type |
      | 1            | 1            | 1            | equilateral   |
      | 1            | 1            | 3            | isosceles     |
      | 1            | 2            | 3            | scalene       |
```
- The scenarios are driven by the values in the table
- Now add, commit and push the code using the commands.
```bash
git add .
git commit -am "Added scenarios outlines"
git push
```